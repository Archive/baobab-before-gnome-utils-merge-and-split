Note
====
Starting from its v.2.4.2, Baobab has been included as a component of 
gnome-utils, so it is no more updated as a stand-alone product, but it will 
follow gnome-utils release cycle.


Copyright
=========

   Baobab - a graphical directory tree analyzer
   Copyright (C) 2005 Fabio Marzocca

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   For more details see the file COPYING.


What is Baobab
==============

	Baobab is able to scan either specific directories or the whole
    filesystem, in order to give the user a graphical tree representation
    including each directory size or percentage in the branch.
	It also includes a complete file-search functionality and auto-detects 
	in real-time any changes made to your home directory as far as any 
	mounted/unmounted device.

    A detailed documentation of the program could be read at:
    http://www.gnome.org/projects/baobab.


System Requirements
===================

Baobab should build on  most unices. It needs the X11R6 (or xorg)
libraries, glib, gtk 2.x, gnome-vfs2, gconf2 libraries.


Author
======

Baobab is being maintained by Fabio Marzocca <thesaltydog@gmail.com>. If
you are having trouble installing and/or running Baobab, feel free to
e-mail me.

You can check on the current status of Baobab via www at: 
	http://www.gnome.org/projects/baobab

Comments, ideas and (most of all) bug reports (and especially patches) are
very welcome.
