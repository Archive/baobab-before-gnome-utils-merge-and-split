# Finnish messages for baobab.
# Copyright (C) 2005 Ilkka Tuohela.
# This file is distributed under the same license as the baobab package.
# Ilkka Tuohela <hile@iki.fi>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: baobab\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-05-06 08:42+0300\n"
"PO-Revision-Date: 2006-05-06 08:44+0300\n"
"Last-Translator: Ilkka Tuohela <hile@iki.fi>\n"
"Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../baobab.desktop.in.in.h:1
msgid "A graphical tool to analyse disk usage"
msgstr "Graafinen työkalu levynkäytön analysointiin"

#: ../baobab.desktop.in.in.h:2 ../baobab.glade.h:5 ../src/callbacks.c:75
msgid "Baobab"
msgstr "Baobab"

#: ../data/baobab.schemas.in.h:1
msgid "A list of partitions to be excluded from scanning."
msgstr "Lista osiosta, joita ei huomioida tarkkailtaessa."

#: ../data/baobab.schemas.in.h:2
msgid "Enable monitoring of home directory"
msgstr "Käytä kotihakemiston tarkkailua"

#: ../data/baobab.schemas.in.h:3
msgid "Exluded partitions"
msgstr "Poissuljetut osiot"

#: ../data/baobab.schemas.in.h:4
msgid "Status Bar is Visible"
msgstr "Tilapalkki näytetään"

#: ../data/baobab.schemas.in.h:5
msgid "Toolbar is Visible"
msgstr "Työkalupalkki näytetään"

#: ../data/baobab.schemas.in.h:6
msgid "Whether any change to the home directory should be monitored."
msgstr "Tarkkailaanko muutoksia kotihakemiston sisältöön."

#: ../data/baobab.schemas.in.h:7
msgid "Whether the status bar at the bottom of main window should be visible."
msgstr "Näytetäänkö tilarivi pääikkunan alareunassa."

#: ../data/baobab.schemas.in.h:8
msgid "Whether the toolbar should be visible in main window."
msgstr "Näytetäänkö työkalupalkki pääikkunassa."

#: ../baobab.glade.h:1
msgid "<b>Search options:</b>"
msgstr "<b>Hakuvaihtoehdot:</b>"

#: ../baobab.glade.h:2
msgid "Advanced"
msgstr "Lisäasetukset"

#: ../baobab.glade.h:3
msgid "Allocated space"
msgstr "Varattu tila"

#: ../baobab.glade.h:4
msgid "Any"
msgstr "Mikä tahansa"

#: ../baobab.glade.h:6
msgid "By _date"
msgstr "_Päiväyksen mukaan"

#: ../baobab.glade.h:7
msgid "By _name"
msgstr "_Nimen mukaan"

#: ../baobab.glade.h:8
msgid "By _size"
msgstr "_Koon mukaan"

#: ../baobab.glade.h:9
msgid "By _type"
msgstr "_Tyypin mukaan"

#: ../baobab.glade.h:10
msgid "Depth level:"
msgstr "Syvyyden taso"

#: ../baobab.glade.h:11
msgid "Directory tree"
msgstr "Kansiopuu"

#: ../baobab.glade.h:12
msgid "Enable auto-detect monitoring of home directory"
msgstr "Käytä kotihakemiston automaattista tarkkailua"

#: ../baobab.glade.h:13
msgid "Enter a file name to search:"
msgstr "Syötä etsittävä tiedostonimi:"

#: ../baobab.glade.h:14
msgid "Exact matching search"
msgstr "Täsmällisen osuman haku"

#: ../baobab.glade.h:15
msgid "Extended search"
msgstr "Laajennettu haku"

#: ../baobab.glade.h:16
msgid "File search"
msgstr "Tiedostohaku"

#: ../baobab.glade.h:17
msgid "Filesystem"
msgstr "Tiedostojärjestelmä"

#: ../baobab.glade.h:18
msgid "Folder"
msgstr "Kansio"

#: ../baobab.glade.h:19
msgid "Last month"
msgstr "Edellinen kuukausi"

#: ../baobab.glade.h:20
msgid "Last week"
msgstr "Edellinen viikko"

#: ../baobab.glade.h:21
msgid "Medium (< 1 MB)"
msgstr "Tavallinen (< 1 Mt)"

#: ../baobab.glade.h:22
msgid "Modification date:"
msgstr "Muokkauspäivä:"

#: ../baobab.glade.h:23
msgid "Scan a _remote folder"
msgstr "Tutki _etäkansiota"

#: ../baobab.glade.h:24
msgid "Scan a remote folder"
msgstr "Tutki etäkansiota"

#: ../baobab.glade.h:25
msgid "Scan a selected f_older"
msgstr "Tutki valittua _kansiota"

#: ../baobab.glade.h:26
msgid "Scan a selected folder"
msgstr "Tutki valittua kansiota"

#: ../baobab.glade.h:27
msgid "Scan preferences"
msgstr "Tutkinnan asetukset"

#: ../baobab.glade.h:28
msgid "Scan the whole _filesystem"
msgstr "Tutki koko _tiedostojärjestelmä"

#: ../baobab.glade.h:29
msgid "Scan the whole filesystem"
msgstr "Tutki koko tiedostojärjestelmä"

#: ../baobab.glade.h:30
msgid "Search"
msgstr "Etsi"

#: ../baobab.glade.h:31
msgid "Search for a file"
msgstr "Etsi tiedostoa"

#: ../baobab.glade.h:32
msgid "Search in selected folder"
msgstr "Etsi valitusta kansiosta"

#: ../baobab.glade.h:33
msgid "Search the whole filesystem"
msgstr "Etsi koko tiedostojärjestelmästä"

#: ../baobab.glade.h:34
msgid "Select"
msgstr "Valitse"

#: ../baobab.glade.h:35
msgid "Select devices to be included during system scan"
msgstr "Valitse järjestelmän tutkinnassa mukana olevat laitteet"

#: ../baobab.glade.h:36
msgid "Show allocated disk usage instead of apparent size"
msgstr "Näytä tarkka levynkäyttö näennäisen sijasta"

#: ../baobab.glade.h:37
msgid "Size:"
msgstr "Koko:"

#: ../baobab.glade.h:38
msgid "Small (< 100 kB)"
msgstr "Pieni (< 100 kt)"

#: ../baobab.glade.h:39
msgid "Sort"
msgstr "Järjestä"

#: ../baobab.glade.h:40
msgid "Sort by name"
msgstr "Järjestä nimen mukaan"

#: ../baobab.glade.h:41
msgid "Sort by size"
msgstr "Järjestä koon mukaan"

#: ../baobab.glade.h:42
msgid "Stop"
msgstr "Pysäytä"

#: ../baobab.glade.h:43
msgid "Stop scanning"
msgstr "Lopeta tutkinta"

#: ../baobab.glade.h:44
msgid "Take a snapshot"
msgstr "Ota kuvakaappaus"

#: ../baobab.glade.h:45
msgid "Total folders:"
msgstr "Kansioita kaikkiaan."

#: ../baobab.glade.h:47
#, no-c-format
msgid "Zoom 100%"
msgstr "Zoomaus 100%"

#: ../baobab.glade.h:48
msgid "Zoom in"
msgstr "Lähennä"

#: ../baobab.glade.h:49
msgid "Zoom out"
msgstr "Loitonna"

#: ../baobab.glade.h:50
msgid "_Actions"
msgstr "_Toiminnot"

#: ../baobab.glade.h:51
msgid "_File"
msgstr "_Tiedosto"

#: ../baobab.glade.h:52
msgid "_Folder graphical map"
msgstr "_Kansion graafinen kartta"

#: ../baobab.glade.h:53
msgid "_Help"
msgstr "O_hje"

#: ../baobab.glade.h:54
msgid "_Search for a file"
msgstr "_Etsi tiedostoa"

#: ../baobab.glade.h:55
msgid "_Sort"
msgstr "_Järjestä"

#: ../baobab.glade.h:56
msgid "_Statusbar"
msgstr "T_ilarivi"

#: ../baobab.glade.h:57
msgid "_Toolbar"
msgstr "_Työkalupalkki"

#: ../baobab.glade.h:58
msgid "_View"
msgstr "_Näytä"

#. set statusbar, percentage and allocated/normal size
#: ../src/baobab.c:168 ../src/callbacks.c:440 ../src/baobab-utils.c:219
msgid "Calculating percentage bars..."
msgstr "Lasketaan prosenttipalkkeja..."

#: ../src/baobab.c:174 ../src/baobab.c:222 ../src/baobab.c:873
#: ../src/callbacks.c:444 ../src/baobab-utils.c:227
msgid "Ready"
msgstr "Valmis"

#: ../src/baobab.c:253
msgid "Unknown"
msgstr "TUntematon"

#: ../src/baobab.c:277
msgid "Full path:"
msgstr "Koko polku:"

#: ../src/baobab.c:278
msgid "Last Modification:"
msgstr "Viimeisin muutos:"

#: ../src/baobab.c:279
msgid "Owner:"
msgstr "Omistaja:"

#: ../src/baobab.c:280
msgid "Allocated bytes:"
msgstr "Varattu tavuja:"

#: ../src/baobab.c:451
msgid "scanning..."
msgstr "tutkitaan..."

#: ../src/baobab.c:481
msgid "<i>Total filesystem usage:</i>"
msgstr "<i>Tiedostojärjestelmän käyttö kaikkiaan:</i>"

#: ../src/baobab.c:534
msgid "contains hardlinks for:"
msgstr "sisältää kovia linkkejä kohteeseen:"

#: ../src/baobab.c:543
#, c-format
msgid "% 5d object"
msgid_plural "% 5d objects"
msgstr[0] "% 5d kohde"
msgstr[1] "% 5d kohdetta"

#: ../src/baobab.c:605
msgid "Invalid UTF-8 characters"
msgstr "Virheellisiä UTF-8-merkkejä"

#: ../src/baobab.c:782
msgid ""
"Cannot initialize GNOME VFS monitoring\n"
"Some real-time auto-detect function will not be available!"
msgstr ""
"Gnome-VFS-tarkkailua ei voida alustaa\n"
"Jotkut reaaliaikaiset ja automaattiset toiminnot eivät ole käytettävissä!"

#: ../src/callbacks.c:76
msgid "A graphical tool to analyse disk usage."
msgstr "Graafinen työkalu levynkäytön analysointiin."

#: ../src/callbacks.c:85
msgid "translator-credits"
msgstr ""
"Ilkka Tuohela, 2006\n"
"\n"
"http://www.gnome.fi/"

#: ../src/callbacks.c:276 ../src/baobab-utils.c:803
msgid "The document does not exist."
msgstr "Asiakirjaa ei löydy"

#: ../src/baobab-treeview.c:83
msgid ""
"<i>Use Menu->Actions->Search for a file, or the search toolbar button.</i>"
msgstr ""
"<i>Etsi tiedostoa valikon kohdasta Toiminnot->Etsi tiedostoa,tai käytä "
"työkalupalkin hakupainiketta</i>"

#: ../src/baobab-utils.c:100
msgid "Select a folder"
msgstr "Valitse kansio"

#. add extra widget
#: ../src/baobab-utils.c:112
msgid "Show hidden folders"
msgstr "Näytä piilokansiot"

#: ../src/baobab-utils.c:192
msgid "Scanning..."
msgstr "Tutkitaan..."

#: ../src/baobab-utils.c:348 ../src/baobab-utils.c:352
#, c-format
msgid "%s is not a valid folder"
msgstr "%s ei ole kelvollinen kansio"

#: ../src/baobab-utils.c:379
msgid "Folder graphical map"
msgstr "Kansion graafinen kartta"

#: ../src/baobab-utils.c:383
msgid "List all files in folder"
msgstr "Näytä kaikki kansion tiedostot"

#: ../src/baobab-utils.c:483
msgid "Please provide a file name to search for!"
msgstr "Syötä haettava tiedostonimi!"

#: ../src/baobab-utils.c:663
msgid "Total filesystem capacity:"
msgstr "Tiedostojärjestelmän koko:"

#: ../src/baobab-utils.c:663
msgid "used:"
msgstr "käytetty:"

#: ../src/baobab-utils.c:664
msgid "available:"
msgstr "vapaana:"

#: ../src/baobab-utils.c:702
msgid "Found:"
msgstr "Löytyi:"

#: ../src/baobab-utils.c:703
msgid "file"
msgstr "tiedosto"

#: ../src/baobab-utils.c:703
msgid "files"
msgstr "tiedostoa"

#: ../src/baobab-utils.c:704
msgid "for total:"
msgstr "kaikkiaan:"

#: ../src/baobab-utils.c:734
msgid "There is no installed viewer capable of displaying the document."
msgstr "Asiakirjan näyttämiseen ei ole asennettu sopivaa katselinta."

#: ../src/baobab-utils.c:796
msgid "Cannot find the Trash on this system!"
msgstr "Roskakoria ei löydy järjestelmästä!"

#: ../src/baobab-utils.c:815
#, c-format
msgid "Moving <b>%s</b> to trash failed: %s."
msgstr "<b>%s</b> siirto roskakoriin epäonnistui: %s."

#: ../src/baobab-utils.c:829
#, c-format
msgid "Do you want to delete <b>%s</b> permanently?"
msgstr "Haluatko poista <b>%s</b> pysyvästi?"

#: ../src/baobab-utils.c:841
#, c-format
msgid "Deleting <b>%s</b> failed: %s."
msgstr "<b>%s</b> poisto epäonnistui: %s."

#: ../src/baobab-utils.c:870
msgid ""
"The content of your home directory has changed.\n"
"Do you want to rescan the last tree to update the folder branch details?"
msgstr ""
"Kotihakemistosi sisältö on muuttunut.\n"
"Haluatko tutkia viimeisimmän kansion päivittääksesi kansionpuun haaran "
"yksityiskohdat?"

#: ../src/baobab-utils.c:891
#, c-format
msgid "Couldn't find pixmap file: %s"
msgstr "Kuvatiedostoa ei löydy: %s"

#: ../src/baobab-prefs.c:146
msgid "Scan"
msgstr "Tutki"

#: ../src/baobab-prefs.c:153
msgid "Device"
msgstr "Laite"

#: ../src/baobab-prefs.c:161
msgid "Mount point"
msgstr "Liitoskohta"

#: ../src/baobab-graphwin.c:156
msgid "Cannot create pixbuf image!"
msgstr "Pixbuf-kuvaa ei voi luoda!"

#: ../src/baobab-graphwin.c:162
msgid "Save the screenshot"
msgstr "Tallenna kuvakaappaus"

#: ../src/baobab-graphwin.c:186
msgid "Image type:"
msgstr "Kuvan tyyppi:"

#: ../src/baobab-graphwin.c:221 ../src/baobab-graphwin.c:314
msgid "Unlimited"
msgstr "Rajoittamaton"

#: ../src/baobab-graphwin.c:276
msgid "Cannot allocate memory for graphical window!"
msgstr "Graafiselle ikkunalle ei voi varata muistia!"

#. Main treemap window
#: ../src/baobab-graphwin.c:304
msgid "Graphical map for folder:"
msgstr "Graafinen kartta kansiosta:"

#: ../src/baobab-remote-connect-dialog.c:125
#, c-format
msgid "\"%s\" is not a valid location."
msgstr "\"%s\" ei ole kelvollinen sijainti."

#: ../src/baobab-remote-connect-dialog.c:134
msgid "Please check the spelling and try again."
msgstr "Tarkista oikeinkirjoitus ja yritä uudestaan."

#: ../src/baobab-remote-connect-dialog.c:160
msgid "You must enter a name for the server."
msgstr "Palvelimen nimi täytyy antaa."

#: ../src/baobab-remote-connect-dialog.c:163
msgid "Please enter a name and try again."
msgstr "Syötä nimi ja yritä uudestaan."

#: ../src/baobab-remote-connect-dialog.c:301
#, c-format
msgid "%s on %s"
msgstr "%s palvelimella %s"

#: ../src/baobab-remote-connect-dialog.c:400
msgid "_Location (URI):"
msgstr "_Sijainti (URI):"

#: ../src/baobab-remote-connect-dialog.c:447
msgid "_Server:"
msgstr "_Palvelin:"

#: ../src/baobab-remote-connect-dialog.c:466
msgid "Optional information:"
msgstr "Lisätietoja:"

#: ../src/baobab-remote-connect-dialog.c:478
msgid "_Share:"
msgstr "_Jako:"

#: ../src/baobab-remote-connect-dialog.c:499
msgid "_Port:"
msgstr "_Portti:"

#: ../src/baobab-remote-connect-dialog.c:519
msgid "_Folder:"
msgstr "_Kansio:"

#: ../src/baobab-remote-connect-dialog.c:539
msgid "_User Name:"
msgstr "_Käyttäjätunnus:"

#: ../src/baobab-remote-connect-dialog.c:560
msgid "_Domain Name:"
msgstr "_Aluenimi:"

#: ../src/baobab-remote-connect-dialog.c:620
msgid "Connect to a remote folder"
msgstr "Yhdistä etäkansioon"

#: ../src/baobab-remote-connect-dialog.c:637
msgid "Service _type:"
msgstr "Palvelun _tyyppi:"

#: ../src/baobab-remote-connect-dialog.c:646
msgid "SSH"
msgstr "SSH"

#: ../src/baobab-remote-connect-dialog.c:648
msgid "Public FTP"
msgstr "Julkinen FTP"

#: ../src/baobab-remote-connect-dialog.c:650
msgid "FTP (with login)"
msgstr "FTP (vaatii kirjatumisen)"

#: ../src/baobab-remote-connect-dialog.c:652
msgid "Windows share"
msgstr "Windows-jako"

#: ../src/baobab-remote-connect-dialog.c:654
msgid "WebDAV (HTTP)"
msgstr "WebDAV (HTTP)"

#: ../src/baobab-remote-connect-dialog.c:656
msgid "Secure WebDAV (HTTPS)"
msgstr "Suojattu WebDAV (HTTPS)"

#: ../src/baobab-remote-connect-dialog.c:658
msgid "Custom Location"
msgstr "Oma sijainti"

#: ../src/baobab-remote-connect-dialog.c:714
msgid "C_onnect"
msgstr "_Yhdistä"

#~ msgid "Preferences"
#~ msgstr "Asetukset"

#~ msgid "Quit"
#~ msgstr "Sulje"

#~ msgid "Scan a selected directory"
#~ msgstr "Tutki valittua kansiota"
