# Spanish translation of Baobab
# This file is distributed under the same license as the Baobab package.
# Copyright (C) 2006 The Free Software Foundation.
# Francisco Javier F. Serrador <serrador@cvs.gnome.org>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: baobab\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-04-21 17:22+0200\n"
"PO-Revision-Date: 2006-04-13 11:48+0200\n"
"Last-Translator: Francisco Javier F. Serrador <serrador@cvs.gnome.org>\n"
"Language-Team: Spanish <traductores@es.gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Spanish\n"
"X-Poedit-Country: SPAIN\n"
"X-Generator: KBabel 1.10.2\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: ../baobab.desktop.in.in.h:1
msgid "A graphical tool to analyse disk usage"
msgstr "Una herramienta gráfica para analizar el uso del disco"

#: ../baobab.desktop.in.in.h:2 ../baobab.glade.h:5 ../src/callbacks.c:75
msgid "Baobab"
msgstr "Baobab"

#: ../data/baobab.schemas.in.h:1
msgid "A list of partitions to be excluded from scanning."
msgstr ""

#: ../data/baobab.schemas.in.h:2
#, fuzzy
msgid "Enable monitoring of home directory"
msgstr "Activar monitorización de auto-detección del directorio personal"

#: ../data/baobab.schemas.in.h:3
msgid "Exluded partitions"
msgstr ""

#: ../data/baobab.schemas.in.h:4
msgid "Status Bar is Visible"
msgstr ""

#: ../data/baobab.schemas.in.h:5
msgid "Toolbar is Visible"
msgstr ""

#: ../data/baobab.schemas.in.h:6
msgid "Whether any change to the home directory should be monitored."
msgstr ""

#: ../data/baobab.schemas.in.h:7
msgid "Whether the status bar at the bottom of main window should be visible."
msgstr ""

#: ../data/baobab.schemas.in.h:8
msgid "Whether the toolbar should be visible in main window."
msgstr ""

#: ../baobab.glade.h:1
msgid "<b>Search options:</b>"
msgstr "<b>Opciones e búsqueda:</b>"

#: ../baobab.glade.h:2
msgid "Advanced"
msgstr "Avanzado"

#: ../baobab.glade.h:3
msgid "Allocated space"
msgstr "Espacio ocupado"

#: ../baobab.glade.h:4
msgid "Any"
msgstr "Cualquiera"

#: ../baobab.glade.h:6
msgid "By _date"
msgstr "por _fecha"

#: ../baobab.glade.h:7
msgid "By _name"
msgstr "por _nombre"

#: ../baobab.glade.h:8
msgid "By _size"
msgstr "por _tamaño"

#: ../baobab.glade.h:9
msgid "By _type"
msgstr "por _tipo"

#: ../baobab.glade.h:10
msgid "Depth level:"
msgstr "Nivel de profundidad:"

#: ../baobab.glade.h:11
msgid "Directory tree"
msgstr "Árbol de directorios"

#: ../baobab.glade.h:12
msgid "Enable auto-detect monitoring of home directory"
msgstr "Activar monitorización de auto-detección del directorio personal"

#: ../baobab.glade.h:13
msgid "Enter a file name to search:"
msgstr "Introduzca un nombre de archivo:"

#: ../baobab.glade.h:14
msgid "Exact matching search"
msgstr "Búsqueda exacta"

#: ../baobab.glade.h:15
msgid "Extended search"
msgstr "Búsqueda extendida"

#: ../baobab.glade.h:16
msgid "File search"
msgstr "Buscar archivo"

#: ../baobab.glade.h:17
msgid "Filesystem"
msgstr ""

#: ../baobab.glade.h:18
#, fuzzy
msgid "Folder"
msgstr "_Carpeta:"

#: ../baobab.glade.h:19
msgid "Last month"
msgstr "Último mes"

#: ../baobab.glade.h:20
msgid "Last week"
msgstr "Última semana"

#: ../baobab.glade.h:21
msgid "Medium (< 1 MB)"
msgstr "Mediano (< 1 Mib)"

#: ../baobab.glade.h:22
msgid "Modification date:"
msgstr "Fecha de modificación:"

#: ../baobab.glade.h:23
msgid "Scan a _remote folder"
msgstr "Inspeccionar una carpeta _remota"

#: ../baobab.glade.h:24
msgid "Scan a remote folder"
msgstr "Inspecciona una carpeta remota"

#: ../baobab.glade.h:25
msgid "Scan a selected f_older"
msgstr "Inspeccionar una _carpeta específica"

#: ../baobab.glade.h:26
msgid "Scan a selected folder"
msgstr "Inspecciona una carpeta seleccionada"

#: ../baobab.glade.h:27
msgid "Scan preferences"
msgstr "Preferencias de inspección"

#: ../baobab.glade.h:28
msgid "Scan the whole _filesystem"
msgstr "Inspeccionar el sistema de _archivos entero"

#: ../baobab.glade.h:29
msgid "Scan the whole filesystem"
msgstr "Inspeccionar el sistema de archivos entero"

#: ../baobab.glade.h:30
msgid "Search"
msgstr ""

#: ../baobab.glade.h:31
msgid "Search for a file"
msgstr "Buscar un archivo"

#: ../baobab.glade.h:32
msgid "Search in selected folder"
msgstr "Buscar en la carpeta seleccionada"

#: ../baobab.glade.h:33
msgid "Search the whole filesystem"
msgstr "Buscar por todo el sistema de ficheros"

#: ../baobab.glade.h:34
msgid "Select"
msgstr "Seleccionar"

#: ../baobab.glade.h:35
msgid "Select devices to be included during system scan"
msgstr ""
"Seleccionar dispositivos para ser incluidos en la inspección del sistema"

#: ../baobab.glade.h:36
msgid "Show allocated disk usage instead of apparent size"
msgstr "Mostrar el uso de disco en zar del tamaño aparente"

#: ../baobab.glade.h:37
msgid "Size:"
msgstr "Tamaño:"

#: ../baobab.glade.h:38
msgid "Small (< 100 kB)"
msgstr "Pequeño (< 100 Kib)"

#: ../baobab.glade.h:39
#, fuzzy
msgid "Sort"
msgstr "_Ordenar"

#: ../baobab.glade.h:40
msgid "Sort by name"
msgstr "Ordenar por nombre"

#: ../baobab.glade.h:41
msgid "Sort by size"
msgstr "Ordenar por tamaño"

#: ../baobab.glade.h:42
msgid "Stop"
msgstr ""

#: ../baobab.glade.h:43
msgid "Stop scanning"
msgstr "Parar la inspección"

#: ../baobab.glade.h:44
msgid "Take a snapshot"
msgstr "Tomar una captura"

#: ../baobab.glade.h:45
msgid "Total folders:"
msgstr "Carpetas totales:"

#: ../baobab.glade.h:47
#, no-c-format
msgid "Zoom 100%"
msgstr "Ampliar al 100%"

#: ../baobab.glade.h:48
msgid "Zoom in"
msgstr "Ampliar"

#: ../baobab.glade.h:49
msgid "Zoom out"
msgstr "Reducir"

#: ../baobab.glade.h:50
msgid "_Actions"
msgstr "A_cciones"

#: ../baobab.glade.h:51
msgid "_File"
msgstr "_Archivo"

#: ../baobab.glade.h:52
msgid "_Help"
msgstr "A_yuda"

#: ../baobab.glade.h:53
msgid "_Search for a file"
msgstr "_Buscar un archivo"

#: ../baobab.glade.h:54
msgid "_Sort"
msgstr "_Ordenar"

#: ../baobab.glade.h:55
msgid "_Statusbar"
msgstr ""

#: ../baobab.glade.h:56
msgid "_Toolbar"
msgstr ""

#: ../baobab.glade.h:57
#, fuzzy
msgid "_View"
msgstr "_Archivo"

#. set statusbar, percentage and allocated/normal size
#: ../src/baobab.c:168 ../src/callbacks.c:451 ../src/baobab-utils.c:219
msgid "Calculating percentage bars..."
msgstr "Calculando barras de porcentajes..."

#: ../src/baobab.c:174 ../src/baobab.c:222 ../src/baobab.c:872
#: ../src/callbacks.c:455 ../src/baobab-utils.c:227
msgid "Ready"
msgstr "Preparado"

#: ../src/baobab.c:253
msgid "Unknown"
msgstr "Desconocido"

#: ../src/baobab.c:277
msgid "Full path:"
msgstr "Ruta completa:"

#: ../src/baobab.c:278
msgid "Last Modification:"
msgstr "Última modificación:"

#: ../src/baobab.c:279
msgid "Owner:"
msgstr "Propietario:"

#: ../src/baobab.c:280
msgid "Allocated bytes:"
msgstr "Bytes ocupados:"

#: ../src/baobab.c:451
msgid "scanning..."
msgstr "inspeccionando..."

#: ../src/baobab.c:481
msgid "<i>Total filesystem usage:</i>"
msgstr "<i>Uso total del sistema de archivos:</i>"

#: ../src/baobab.c:534
msgid "contains hardlinks for:"
msgstr "contiene enlaces duros de:"

#: ../src/baobab.c:543
#, c-format
msgid "% 5d object"
msgid_plural "% 5d objects"
msgstr[0] "% 5d objeto"
msgstr[1] "% 5d objetos"

#: ../src/baobab.c:605
msgid "Invalid UTF-8 characters"
msgstr "Caracteres UTF-8 inválidos"

#: ../src/baobab.c:782
msgid ""
"Cannot initialize GNOME VFS monitoring\n"
"Some real-time auto-detect function will not be available!"
msgstr ""
"No se puede inicializar la monitorización Gnome VFS\n"
"Alguna función de auto--detección en tiempo real no estará disponible"

#: ../src/callbacks.c:76
msgid "A graphical tool to analyse disk usage."
msgstr "Una herramienta gráfica para analizar el espacio de disco."

#: ../src/callbacks.c:85
msgid "translator-credits"
msgstr "Francisco Javier F. Serrador <serrador@cvs.gnome.org>"

#: ../src/callbacks.c:287 ../src/baobab-utils.c:817
msgid "The document does not exist."
msgstr "El documento no existe."

#: ../src/baobab-treeview.c:82
msgid ""
"<i>Use Menu->Actions->Search for a file, or the search toolbar button.</i>"
msgstr ""
"<i>Use Menú->Acciones->Buscar archivo, o el botón de búsqueda de la barra de "
"herramientas.</i>"

#: ../src/baobab-utils.c:100
msgid "Select a folder"
msgstr "Selecciona una carpeta"

#. add extra widget
#: ../src/baobab-utils.c:112
msgid "Show hidden folders"
msgstr "Mostrar carpetas ocultas"

#: ../src/baobab-utils.c:192
msgid "Scanning..."
msgstr "Inspeccionando..."

#: ../src/baobab-utils.c:348 ../src/baobab-utils.c:352
#, c-format
msgid "%s is not a valid folder"
msgstr "%s no es una carpeta válida"

#: ../src/baobab-utils.c:393
msgid "Folder graphical map"
msgstr "Mapa gráfico de carpetas"

#: ../src/baobab-utils.c:397
msgid "List all files in folder"
msgstr "Listar todos los archivos de la carpeta"

#: ../src/baobab-utils.c:497
msgid "Please provide a file name to search for!"
msgstr "Proporcione un nombre con el que buscar"

#: ../src/baobab-utils.c:677
msgid "Total filesystem capacity:"
msgstr "Capacidad total del sistema de archivos:"

#: ../src/baobab-utils.c:677
msgid "used:"
msgstr "usado:"

#: ../src/baobab-utils.c:678
msgid "available:"
msgstr "disponible:"

#: ../src/baobab-utils.c:716
msgid "Found:"
msgstr "Encontrado:"

#: ../src/baobab-utils.c:717
msgid "file"
msgstr "archivo"

#: ../src/baobab-utils.c:717
msgid "files"
msgstr "archivos"

#: ../src/baobab-utils.c:718
msgid "for total:"
msgstr "de un total:"

#: ../src/baobab-utils.c:748
msgid "There is no installed viewer capable of displaying the document."
msgstr "No hay instalado un visor que pueda ver el documento."

#: ../src/baobab-utils.c:810
msgid "Cannot find the Trash on this system!"
msgstr "Nos e pudo encontrar la papelera en este sistema"

#: ../src/baobab-utils.c:828
#, c-format
msgid "Moving <b>%s</b> to trash failed: %s."
msgstr "Ha fallado al mover <b>%s</b> a la papelera: %s."

#: ../src/baobab-utils.c:840
#, c-format
msgid "Do you want to delete <b>%s</b> permanently?"
msgstr "¿Quiere borrar <b>%s</b> permanentemente?"

#: ../src/baobab-utils.c:852
#, c-format
msgid "Deleting <b>%s</b> failed: %s."
msgstr "Ha fallado el borrado de <b>%s</b>: %s."

#: ../src/baobab-utils.c:880
msgid ""
"The content of your home directory has changed.\n"
"Do you want to rescan the last tree to update the folder branch details?"
msgstr ""
"El contenido de su directorio personal ha cambiado.\n"
"¿Quiere inspeccionar el último árbol para actualizar los detalles de la rama "
"de la carpeta?"

#: ../src/baobab-utils.c:901
#, c-format
msgid "Couldn't find pixmap file: %s"
msgstr "No se pudo encontrar un archivo de imagen: %s"

#: ../src/baobab-prefs.c:146
msgid "Scan"
msgstr "Inspeccionar"

#: ../src/baobab-prefs.c:153
msgid "Device"
msgstr "Dispositivo"

#: ../src/baobab-prefs.c:161
msgid "Mount point"
msgstr "Punto de montaje"

#: ../src/baobab-graphwin.c:156
msgid "Cannot create pixbuf image!"
msgstr "No se puede crear la imagen pixbuf"

#: ../src/baobab-graphwin.c:162
msgid "Save the screenshot"
msgstr "Guardar la captura"

#: ../src/baobab-graphwin.c:186
msgid "Image type:"
msgstr "Tipo de imagen:"

#: ../src/baobab-graphwin.c:221 ../src/baobab-graphwin.c:314
msgid "Unlimited"
msgstr "ilimitado"

#: ../src/baobab-graphwin.c:276
msgid "Cannot allocate memory for graphical window!"
msgstr "No se puede reservar memoria para la ventana gráfica"

#. Main treemap window
#: ../src/baobab-graphwin.c:304
msgid "Graphical map for folder:"
msgstr "Mapa gráfico para la carpeta:"

#: ../src/baobab-remote-connect-dialog.c:125
#, c-format
msgid "\"%s\" is not a valid location."
msgstr "«%s» no es un lugar válido."

#: ../src/baobab-remote-connect-dialog.c:134
msgid "Please check the spelling and try again."
msgstr "Por favor compruebe lo que ha escrito e inténtelo de nuevo."

#: ../src/baobab-remote-connect-dialog.c:160
msgid "You must enter a name for the server."
msgstr "Debe introducir un nombre para el servidor."

#: ../src/baobab-remote-connect-dialog.c:163
msgid "Please enter a name and try again."
msgstr "Por favor, introduzca un nombre e inténtelo de nuevo."

#: ../src/baobab-remote-connect-dialog.c:301
#, c-format
msgid "%s on %s"
msgstr "%s en %s"

#: ../src/baobab-remote-connect-dialog.c:400
msgid "_Location (URI):"
msgstr "_Lugar (URI):"

#: ../src/baobab-remote-connect-dialog.c:447
msgid "_Server:"
msgstr "_Servidor:"

#: ../src/baobab-remote-connect-dialog.c:466
msgid "Optional information:"
msgstr "Información opcional:"

#: ../src/baobab-remote-connect-dialog.c:478
msgid "_Share:"
msgstr "_Compartición:"

#: ../src/baobab-remote-connect-dialog.c:499
msgid "_Port:"
msgstr "_Puerto:"

#: ../src/baobab-remote-connect-dialog.c:519
msgid "_Folder:"
msgstr "_Carpeta:"

#: ../src/baobab-remote-connect-dialog.c:539
msgid "_User Name:"
msgstr "_Usuario:"

#: ../src/baobab-remote-connect-dialog.c:560
msgid "_Domain Name:"
msgstr "_Dominio:"

#: ../src/baobab-remote-connect-dialog.c:620
msgid "Connect to a remote folder"
msgstr "Conectar a una carpeta remota"

#: ../src/baobab-remote-connect-dialog.c:637
msgid "Service _type:"
msgstr "_Tipo de servicio:"

#: ../src/baobab-remote-connect-dialog.c:646
msgid "SSH"
msgstr "SSH"

#: ../src/baobab-remote-connect-dialog.c:648
msgid "Public FTP"
msgstr "FTP público"

#: ../src/baobab-remote-connect-dialog.c:650
msgid "FTP (with login)"
msgstr "FTP (con entrada)"

#: ../src/baobab-remote-connect-dialog.c:652
msgid "Windows share"
msgstr "Compartición Windows"

#: ../src/baobab-remote-connect-dialog.c:654
msgid "WebDAV (HTTP)"
msgstr "WebDAV (HTTP)"

#: ../src/baobab-remote-connect-dialog.c:656
msgid "Secure WebDAV (HTTPS)"
msgstr "WebDAV seguro (HTTPS)"

#: ../src/baobab-remote-connect-dialog.c:658
msgid "Custom Location"
msgstr "Lugar personalizado"

#: ../src/baobab-remote-connect-dialog.c:714
msgid "C_onnect"
msgstr "C_onectar"

#~ msgid "Preferences"
#~ msgstr "Preferencias"

#~ msgid "Quit"
#~ msgstr "Salir"

#~ msgid "Scan a selected directory"
#~ msgstr "Inspecciona un directorio seleccionado"

#~ msgid "%s is a symbolic link"
#~ msgstr "%s es un enlace simbólico"

#~ msgid "objects"
#~ msgstr "objetos"

#~ msgid ""
#~ "A new script has been generated to open Baobab from Nautilus. Just right-"
#~ "click in Nautilus->Script to access Baobab. "
#~ msgstr ""
#~ "Se ha generado un script para abrir Baobab desde Nautilus. Tan sólo tiene "
#~ "que pulsar con el botón derecho en Nautilus->Script para acceder a Baobab."

#~ msgid "Open in Baobab..."
#~ msgstr "Abrir en Baobab..."
